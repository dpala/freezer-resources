# Freezer: A Specialized NVM Backup Controller for Intermittently-Powered Systems

This repository contains the notebooks, the code and the resources for our paper:
*Freezer: A Specialized NVM Backup Controller for Intermittently-Powered Systems*

## Structure of the repository
- *./notebooks*
    + *Single_trace_backup_strategy_eval.ipynb*: analysis of different backup strategies on a single benchmark trace.
    + *Multi_trace_result_eval.ipynb*: analysis of average results of backup strategies on multiple benchmark traces. 
    + *random intervals.ipynb*: analysis of randomly distributed power failures on backup strategies.
- *./traces*: contains the memory access traces for all benchmarks.
- *./res*: resources folder where dataframes are saved.
    + *backup_strategy_cmp*: contains resources needed for *Multi_trace_result_eval.ipynb* notebook. 
        * *interval_6*: contains for each benchmark a file whith the results of each backup strategy, using intervals of 10^6 cycles. 
        * *interval_7*: contains for each benchmark a file whith the results of each backup strategy, using intervals of 10^7 cycles. 
        * ...
    + *random_intervals*: contains resources needed for *random intervals.ipynb* notebook.
        * *poisson_1e-6_iv_100-executions.csv*: file containing for each benchmark the average result of 100 executions with randomly distributed power failures, with an expected failure rate of 1 failure over 10^6 cycles.
        * ...
- *./scripts*: contains the scripts for automating data collections and running the analysis in batches.
    + *./scripts/mem_access_energy.py*: script for memory access energy simulation and comparison.

## Analysis Flow
### Single trace analysis
First benchmarks memory access traces are analysed in "./notebooks/Single_trace_backup_strategy_eval.ipynb".
This notebook loads one trace and simulates power failures dividing the trace into intervals.
Then for each interval it simulates each backup strategy described in the paper, computing the size of a backup for each inteval, as shown in the table (for fft benchmark):

| Interval | Oracle modified | Full Memory | Used (block 1) | Used (block 8) | Modified (block 1) | Modified (block 8) | 
|----------|-----------------|-------------|----------------|----------------|--------------------|--------------------| 
| 0        | 149             | 3200        | 335            | 512            | 213                | 272                | 
| 1        | 120             | 3200        | 313            | 384            | 144                | 176                | 
| 2        | 95              | 3200        | 307            | 368            | 138                | 160                | 
| 3        | 320             | 3200        | 1353           | 1616           | 835                | 1000               | 
| 4        | 70              | 3200        | 707            | 920            | 528                | 656                | 
| 5        | 105             | 3200        | 797            | 1024           | 545                | 680                | 
| 6        | 0               | 3200        | 675            | 1008           | 496                | 728                | 

### Multi-trace analysis
This type of results can be collected for each benchmark memory access trace and for different interval sizes.
The folder *./res/backup_strategy_cmp/* contains grouped by interval size, the files with the result of the different backup strategies on each of the benchmarks.
The *Multi_trace_result_eval.ipynb* notebook uses these files to compare and extract averages and other aggregate informations on the backup strategies and the different benchmarks.

### Random Intervals
The *random intervals.ipynb* notebook was used to analyse the impact of randomly distributed power failures on the backup strategies, istead of fixed size intervals.

### Energy
The *Energy.ipynb* notebook is used for the analysis of memory access energy and the comparison of three system models:

- SRAM + NVM
- NVM Only
- Cache + NVM

The notebook is divided into 3 sections:
- Single trace
- Multi trace
- NVSim data

In the first two sections the energy cost is modelled in a simplified way, using 1 as the cost of SRAM read and writes, 1 for NVM reads and 10 or 100 for NVM writes.
The tird section present a more acurate analysis using data extracted from NVSim for the read and write energies of SRAM, NVMs (STT and RRAM) and the caches.

The data used in the third part of the notebook is stored in *res/energy/energy_1e-6iv.csv*, and was obtained using the equations described in the paper.
The file *res/energy/energy_1e-6iv.csv* can be obtained running the script *script/mem_access_energy.py*

## Requirements
- python3
- jupyter
- pandas
- numpy
- pint

